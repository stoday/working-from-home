
import pyautogui
import schedule
import time
import numpy as np

pyautogui.FAILSAFE = False


def shake_mouse():
    pyautogui.moveTo(100, 200)
    pyautogui.moveTo(100, 300)
    print('shaking the mouse!')
    pyautogui.press('esc')


def move_and_click():
    try:
        start_pos = pyautogui.locateOnScreen('.\pics\windows_start.png', confidence=.8)
        start_center_0 = np.floor(start_pos[0] + start_pos[2] / 2)
        start_center_1 = np.floor(start_pos[1] + start_pos[3] / 2)
        pyautogui.moveTo(start_center_0, start_center_1)
        pyautogui.click()
        time.sleep(1)

        icon_pos = pyautogui.locateOnScreen('.\pics\teams_icon.png', confidence=.9)
        icon_center_0 = np.floor(icon_pos[0] + icon_pos[2] / 2)
        icon_center_1 = np.floor(icon_pos[1] + icon_pos[3] / 2)
        pyautogui.moveTo(icon_center_0, icon_center_1)
        pyautogui.click()
        time.sleep(3)

        group2_pos = pyautogui.locateOnScreen('.\pics\teams_group2.png', confidence=.9)
        group2_center_0 = np.floor(group2_pos[0] + group2_pos[2] / 2)
        group2_center_1 = np.floor(group2_pos[1] + group2_pos[3] / 2)
        pyautogui.moveTo(group2_center_0, group2_center_1)
        pyautogui.click()
        time.sleep(1)

    except Exception as err:
        print(err)


def say_goodbye():
    try:
        move_and_click()
        pyautogui.write('I am getting off work.')
        time.sleep(1)

        pyautogui.press('enter')
        time.sleep(1)

    except Exception as err:
        print(err)


def say_hi():
    move_and_click() 
    pyautogui.write('I am working.')
    time.sleep(1)

    pyautogui.press('enter')
    time.sleep(1)


if __name__ == '__main__':
    print('Running...')
    schedule.every(10).minutes.do(shake_mouse)

    schedule.every().monday.at('09:00').do(say_hi)
    schedule.every().tuesday.at('09:00').do(say_hi)
    schedule.every().wednesday.at('09:00').do(say_hi)
    schedule.every().thursday.at('09:00').do(say_hi)
    schedule.every().friday.at('09:00').do(say_hi)

    schedule.every().monday.at('18:00').do(say_goodbye)
    schedule.every().tuesday.at('18:00').do(say_goodbye)
    schedule.every().wednesday.at('18:00').do(say_goodbye)
    schedule.every().thursday.at('18:00').do(say_goodbye)
    schedule.every().friday.at('18:00').do(say_goodbye)

    while True: 
        schedule.run_pending() 
        time.sleep(60) 

